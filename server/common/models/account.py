from common.app import db
from common.models.public_key import PublicKey
from common.models.guid import GUID

import uuid

class Account(db.Model):
    # The id of an account is also the id of its corresponding message group
    id = db.Column(GUID, db.ForeignKey('group.id'), primary_key=True, nullable=False)
    username = db.Column(db.String(20), unique=True)
    key_id = db.Column(GUID, db.ForeignKey('public_key.id'))

    state = db.Column(db.String())
    root_key_ciphertext = db.Column(db.String())

    key = db.relationship('PublicKey', backref=db.backref('accounts', lazy='dynamic'))

    group = db.relationship('Group')

    def __init__(self, group, username, key, root_key_ciphertext=None, state=None):
        self.group = group
        self.username = username
        self.root_key_ciphertext = root_key_ciphertext
        self.key = key
        self.state = state
