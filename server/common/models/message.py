from common.app import db
from common.models.guid import GUID
import datetime
import uuid

class Message(db.Model):
    id = db.Column(GUID, primary_key=True, default=uuid.uuid4)
    cryptotext = db.Column(db.String(25000))

    # The order field was dropped in favor of timestamps
    timestamp = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    sender_id = db.Column(db.ForeignKey('account.id'))
    recipient_id = db.Column(db.ForeignKey('group.id'))
    key_id = db.Column(db.ForeignKey('public_key.id'))

    sender = db.relationship('Account', backref=db.backref('messages_sent', lazy='dynamic'))

    recipient = db.relationship('Group', backref=db.backref('messages_received', lazy='dynamic'))

    key = db.relationship('PublicKey', backref=db.backref('public_keys', lazy='dynamic'))

    def __init__(self, cryptotext, sender, recipient, key):
        self.cryptotext = cryptotext
        self.sender = sender
        self.recipient = recipient
        self.key = key
