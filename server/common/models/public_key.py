from common.app import db
from common.models.guid import GUID
import uuid

class PublicKey(db.Model):
    id = db.Column(GUID, primary_key=True, default=uuid.uuid4)
    rsa_pub = db.Column(db.String(length=5096))

    def __init__(self, rsa_pub):
        self.rsa_pub = rsa_pub
