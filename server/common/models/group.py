from common.app import db
from common.models.guid import GUID
import uuid

class Group(db.Model):
    id = db.Column(GUID, primary_key=True, default=uuid.uuid4)

    def __init__(self):
        pass
