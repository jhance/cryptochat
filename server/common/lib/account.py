from common.models.account import Account
from common.models.group import Group
from common.models.public_key import PublicKey

from common.app import db

def make_account(username, rsa_pub, **kwargs):
    group = Group()
    public_key = PublicKey(rsa_pub=rsa_pub)

    db.session.add(group)
    db.session.add(public_key)

    account = Account(group, username, key=public_key, **kwargs)
    db.session.add(account)

    return account
