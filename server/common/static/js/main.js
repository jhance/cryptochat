'use strict';

var _api_target = 'http://127.0.0.1:5000/';
var _websocket_target = 'http://127.0.0.1:5005';

this.ui = (function() {
    var ui = {};

    ui.show_overlay = function () {
        $('#overlay').removeClass('toggle_hidden');
    };

    ui.hide_overlay = function () {
        $('#overlay').addClass('toggle_hidden');
        $('.group_mem_select').off('click');
    };

    ui.toggle_login = function () {
        $('.login_mode').toggleClass('toggle_hidden');
    };

    ui.set_login_status = function (string) {
        if (string) {
            $('#login_status').text(string).removeClass('toggle_hidden');
        } else {
            $('#login_status').addClass('toggle_hidden');
        }
    };

    ui.show_group_options = function (group_id, default_name) {
        $('#setup').addClass('hidden_overlay');
        $('#group_settings').removeClass('hidden_overlay');

        $('#group_name').val(default_name),

        $('.group_mem_select').click(function (e) {
            $(e.target).toggleClass('group_mem_toggled');
        });

        $(document).off('keyup');
        $(document).keyup(function(e) {
            if (e.keyCode == 27) {
                ui.hide_overlay();
            }
        });


        $('#group_settings_create').off('click');
        $('#group_settings_create').click(function (e) {
            window.session.group_settings_create(group_id);
        });

        ui.show_overlay();
    };

    ui.stringToColor = function (string) {
        var hash = 3;
        for (var i = 0; i < string.length; i++) {
            hash = ((hash * 2 + string.charCodeAt(i)) | 0) % 360;
        }
        return "hsl(" + hash + ",100%,70%)";
    };

    ui.display_message = function (target, name, message, is_status, is_unsent) {
        var chat = $('<div class="chat_message ' + (is_unsent ? 'unsent' : '') + '"><div></div><div></div></div>');
        var child1 = chat.children()[0];
        var child2 = chat.children()[1];

        child1.textContent = name;
        child1.style.color = ui.stringToColor(name);
        child2.textContent = message;

        if (is_status) {
            child1.remove();
            child2.style.color = '#666';
        }

        target.append(chat);
        target.scrollTop(target[0].scrollHeight);

        return chat;
    };

    ui.set_user_string = function (string) {
        if (string) {
            $('#current_user_info strong')[0].textContent = string;
            $('#current_user_info').removeClass('toggle_hidden');
        } else {
            $('#current_user_info').addClass('toggle_hidden');
        }
    }

    ui.add_chat = function (id, title) {
        var div_id = 'chat' + id;
        if ($('#' + div_id)[0] != null) {
            return;
        }

        $('#active_chats').append('<div id="' + div_id + '" class="chat_tab">' + title + '</div>');
        $('#' + div_id).click(function () {
            session.set_active_chat(id);
        });
    };

    return ui;
})();

this.api = (function() {
    var api = {};

    api.get_state = function (id) {
        console.log('getting state with id = '+id)
        return $.get(_api_target + 'state/' + id).then(x => x.state);
    };

    api.post_state = function (id, state) {
        var json = JSON.stringify({
            state: state,
        });
        return async_post(
            _api_target + 'state/' + id,
            json
        ).then(function (x) {
            console.log('posted state to server');
            return x;
        });
    };

    api.get_public_key = function (id) {
        return $.get(_api_target + 'keys/public/' + id)
            .then(data => data.public_key);
    };

    api.get_private_key = function (id) {
        return $.get(_api_target + 'keys/private/' + id)
            .then(data => data.root_key_ciphertext);
    };

    api.get_userid = function (username) {
        return $.get(_api_target + 'user/id/' + username);
    };

    api.get_username = function (userid) {
        return $.get(_api_target + 'user/name/' + userid);
    };

    api.create_user = function (username, public_key, private_key) {
        var json = JSON.stringify({
            username: username,
            public_key: public_key,
            root_key_ciphertext: private_key,
        });
        return async_post(
            _api_target + 'user/create',
            json
        ).then(data => data.id);
    };

    api.create_group = function () {
        return async_post(
            _api_target + 'groups/id',
            {}
        ).then(data => data.id);
    };

    api.create_group_key_id = function () {
        return async_post(
            _api_target + 'groups/key',
            {}
        ).then(data => data.id);
    };

    function timestamp_or_now(timestamp) {
        if (timestamp) {
            return timestamp;
        }
        timestamp = new Date();
        timestamp.setMinutes(timestamp.getMinutes() + timestamp.getTimezoneOffset());
        return timestamp.getTime() / 1000;
    }

    api.get_stored_messages = function (id, timestamp) {
        timestamp = timestamp_or_now(timestamp);

        return $.get(_api_target + 'messages/' + id + '/' + timestamp)
            .then(function (data) {
                data = data.messages;
                return data;
            });
    };

    api.get_stored_convo = function (user1, user2, timestamp) {
        timestamp = timestamp_or_now(timestamp);

        return $.get(_api_target + 'conversations/' + user1 + '/' + user2 + '/' + timestamp)
            .then(function (data) {
                data = data.messages;
                return data;
            });
    };

    function async_post(url, data) {
        return $.ajax({
            type: 'POST',
            url: url,
            data: data,
            contentType: 'application/json;charset=UTF-8'
        });
    }

    return api;
})();

this.session = (function() {
    var current = {};

    current.user = null;
    current.user_id = null;
    current.passphrase = null;
    current.public_key = null;
    current.private_key = null;
    current.active_chat_id = null;
    current.active_chats = {};

    var state = { friends: {}, groups: {} };
    var state_ready = true;

    function seed_gen() {
        return 'seed' + (Math.random() * 100000); // lol
    }

    function gen_random_symmetric_key() {
        return base64js.fromByteArray(openpgp.crypto.hash.sha512(seed_gen()));
    }

    function make_passphrase(username, password) {
        // TODO: This is pretty awful. Probably should be more secure.
        return openpgp.crypto.hash.sha512(username + password);
    }

    function set_current_user(username, user_id, password) {
        current.user = username;
        current.user_id = user_id;
        current.passphrase = make_passphrase(username, password);
    }

    /**
     * Encrypts a byte array with the current passphrase. Returns a base64
     * encoded string.
     */
    function passphrase_encrypt(data) {
        return sym_encrypt(current.passphrase, data);
    }

    /**
     * Decrypts a base64 encoded string with the current passphrase. Returns a
     * cleartext string.
     */
    function passphrase_decrypt(data) {
        return sym_decrypt(current.passphrase, data);
    }

    /**
     * Encrypts a byte array with the given key. The result is a base64
     * encoded string.
     */
    function sym_encrypt(key, data) {
        var options = {
            data: data,
            passwords: [key],
            armor: false,
        };

        return openpgp.encrypt(options)
            .then(encrypted => base64js.fromByteArray(encrypted.message.packets.write()));
    }

    /**
     * Decrypts a base64 encoded string with the given key. Returns a cleartext
     * string.
     */
    function sym_decrypt(key, data) {
        var options = {
            message: openpgp.message.read(base64js.toByteArray(data)),
            password: key,
        };

        return openpgp.decrypt(options).then(data => data.data);
    }

    function pgp_encrypt(key, data) {
        var keys = openpgp.key.readArmored(key).keys.concat(openpgp.key.readArmored(current.public_key).keys);
        var options = {
            data: data,
            publicKeys: keys,
            privateKeys: [current.private_key],
        };

        return openpgp.encrypt(options).then(ciphertext => ciphertext.data);
    }

    function pgp_decrypt(sign_key, data) {
        var options = {
            message: openpgp.message.readArmored(data),
            publicKeys: openpgp.key.readArmored(sign_key).keys,
            privateKey: current.private_key,
        };

        return openpgp.decrypt(options).then(plaintext => plaintext.data);
    }

    function fetch_state() {
        return window.api.get_state(current.user_id)
        .then(function (data) {
            if (data === null) {
                return new Promise(function (resolve, reject) {
                    resolve(state);
                });
            }
            return passphrase_decrypt(data)
            .then(data => JSON.parse(data).state);
        });
    }

    function post_state() {
        if (!state_ready) {
            return;
        }

        return passphrase_encrypt(JSON.stringify({state: state}))
        .then(function (cyphertext) {
            return window.api.post_state(current.user_id, cyphertext);
        });
    }

    function get_stored_messages(id) {
        var p;

        if (id in state.friends) {
            p = window.api.get_stored_convo(current.user_id, id);
        } else {
            p = window.api.get_stored_messages(id);
        }

        p.then(function (messages) {
            display_messages_in_order(id, messages);
        });
    }

    function display_messages_in_order(id, messages, i) {
        i = i ? i : 0;

        if (i >= messages.length) {
            return;
        }

        var message = messages[i];
        session.receive_message(message.sender_id, id, message.key_id, message.cryptotext, true)
        .then(function (last_message) {
            return display_messages_in_order(id, messages, i + 1);
        });
    }

    var session = {};

    session.login = function (username, password) {
        window.api.get_userid(username)
            .done(function (data) {
                set_current_user(username, data.id, password);

                window.api.get_public_key(data.key_id)
                .done(function (public_key) {
                    current.public_key = public_key;
                    window.api.get_private_key(current.user_id)
                    .done(function (ciphertext) {
                        passphrase_decrypt(ciphertext)
                        .then(function (cleartext) {
                            current.private_key = openpgp.key.readArmored(cleartext).keys[0];
                            current.private_key.decrypt(current.passphrase);

                            state_ready = false;
                            fetch_state().then(function (last_state) {
                                state = last_state;

                                for (var friend in state.friends) {
                                    add_new_chat(state.friends[friend]);
                                    get_stored_messages(friend);
                                }

                                for (var group in state.groups) {
                                    add_new_chat(state.groups[group]);
                                    session.websocket.socket.emit('listen', { ids: [group] });
                                    get_stored_messages(group);
                                }

                                state_ready = true;
                                session.set_active_chat(null);

                                return state;
                            });

                            window.ui.hide_overlay();
                            window.ui.set_user_string(username);
                            session.websocket.connect();
                        }).catch(function (e) {
                            window.ui.set_login_status('Incorrect password for user!');
                        });
                    })
                    .fail(function () {
                        window.ui.set_login_status('Couldn\'t load private key!');
                    });
                })
                .fail(function () {
                    window.ui.set_login_status('Couldn\'t load public key!');
                });
            })
            .fail(function (data) {
                window.ui.set_login_status('Couldn\'t find username!');
            });
    };

    session.register = function (username, password, verify) {
        if (password != verify) {
            window.ui.set_login_status('Passwords don\'t match!');
            return;
        }

        set_current_user(username, null, password);

        var options = {
            userIds: [{ name: username }],
            numBits: 2048,
            passphrase: current.passphrase,
        };

        window.ui.set_login_status('Generating new key...');

        openpgp.generateKey(options).then(function (key) {
            current.public_key = key.publicKeyArmored;
            var private_key_str = key.privateKeyArmored;
            current.private_key = openpgp.key.readArmored(private_key_str).keys[0];
            current.private_key.decrypt(current.passphrase);

            window.ui.set_login_status('Creating new account...');

            passphrase_encrypt(private_key_str)
            .then(function (ciphertext) {
                window.api.create_user(username, current.public_key, ciphertext)
                .done(function (data) {
                    current.user_id = data;
                    window.ui.hide_overlay();
                    window.ui.set_user_string(username);
                    session.websocket.connect();
                })
                .fail(function () {
                    window.ui.set_login_status('Couldn\'t create account!');
                });
            });
        });
    };

    function get_active_chat_panel() {
        return current.active_chats[current.active_chat_id];
    }

    session.send_message = function (text) {
        if (text == '' || current.active_chat_id == null) {
            return;
        }
        var chat_div = window.ui.display_message(get_active_chat_panel(), current.user, text, false, true);

        session.dispatch_message(current.active_chat_id, 'message', text)
        .then(function () {
            chat_div.removeClass('unsent');
        });
    };

    session.send_group_invite = function (target, group_id, update) {
        var group = state.groups[group_id];

        group.is_update = update;
        session.dispatch_message(
            target,
            'group_add',
            JSON.stringify(group)
        );

        window.ui.display_message(
            current.active_chats[target],
            '',
            'You added ' + state.friends[target].name + ' to ' + group.name,
            true
        );
    };

    session.dispatch_message = function (target, type, data) {
        var content = {
            type: type,
            data: data,
        };

        return session.websocket.send(target, JSON.stringify(content));
    };

    session.receive_message = function (sender_id, recipient_id, key_id, cryptotext, old_message) {
        var p = null;
        var sender_name;
        var target_id;

        if (recipient_id == current.user_id || recipient_id == sender_id) {
            // second conditional clause is triggered by loading old PMs sent
            // to the current user
            if (sender_id in state.friends) {
                sender_name = state.friends[sender_id].name;
                p = session.get_friend_key(sender_id);
            } else {
                p = session.add_friend_id(sender_id)
                .then(function (friend) {
                    sender_name = friend.name;
                    return session.get_friend_key(friend.id);
                });
            }
            target_id = sender_id;
            p = p.then((public_key) => pgp_decrypt(public_key, cryptotext));
        } else  if (sender_id == current.user_id && recipient_id in state.friends) {
            // condition of loading message current user sent as a PM
            sender_name = current.user;
            target_id = recipient_id;
            p = pgp_decrypt(current.public_key, cryptotext);
        } else {
            if (recipient_id in state.groups) {
                var group = state.groups[recipient_id];
                sender_name = group.members[sender_id].name;
                target_id = recipient_id;
                p = sym_decrypt(group.keys[key_id], cryptotext);
            } else {
                console.log("Received message for unknown group " + recipient_id);
                return;
            }
        }

        return p.then(function (cleartext) {
            var message = JSON.parse(cleartext);

            if (message.type == 'message') {
                window.ui.display_message(
                    current.active_chats[target_id],
                    sender_name,
                    message.data
                );

                if (!old_message && target_id != current.active_chat_id) {
                    $('#chat' + target_id).addClass('notification');
                }
            } else if (message.type == 'group_add') {
                message.data = JSON.parse(message.data);

                message.data.members[current.user_id] = {
                    id: current.user_id,
                    name: current.user,
                };

                if (message.data.is_update) {
                    // Updating existing group chat
                    var group = state.groups[message.data.id];

                    var removed = [];
                    var added = [];

                    for (var id in group.members) {
                        if (!(id in message.data.members)) {
                            removed.push(id);
                        }
                    }

                    if (removed.length > 0) {
                        window.ui.display_message(
                            current.active_chats[message.data.id],
                            sender_name,
                            sender_name + ' removed ' + removed.length + (removed.length > 1 ? ' users' : ' user'),
                            true
                        );
                    }

                    for (var id in message.data.members) {
                        if (!(id in group.members)) {
                            added.push(id);
                        }
                    }

                    if (added.length > 0) {
                        window.ui.display_message(
                            current.active_chats[message.data.id],
                            sender_name,
                            sender_name + ' added ' + removed.length + (removed.length > 1 ? ' users' : ' user'),
                            true
                        );
                    }

                    if (!old_message) {
                        // Update group in state and post updated state
                        state.groups[message.data.id] = message.data;
                        post_state();

                        if (message.data.id == current.active_chat_id) {
                            session.set_active_chat(null);
                            session.set_active_chat(message.data.id);
                        }
                    }
                } else {
                    // New group chat
                    window.ui.display_message(
                        current.active_chats[target_id],
                        sender_name,
                        sender_name + ' added you to ' + message.data.name,
                        true
                    );

                    if (!old_message) {
                        state.groups[message.data.id] = message.data;
                        add_new_chat(message.data);
                        session.websocket.socket.emit('listen' , { ids: [message.data.id] });
                    }
                }

                post_state();
            } else if (message.type == 'leave') {
                window.ui.display_message(
                    current.active_chats[target_id],
                    sender_name,
                    sender_name + ' left chat',
                    true
                );

                if (!old_message) {
                    if (target_id in state.groups) {
                        delete state.groups[target_id].members[sender_id];
                        post_state();

                        if (target_id == current.active_chat_id) {
                            session.set_active_chat(null);
                            session.set_active_chat(message.data.id);
                        }
                    }
                }
            }

            return cleartext;
        });
    };
    
    // should only be called during testing
    session.get_current_data = function () {
        return current;
    }

    session.get_current_username = function (id) {
        return current.user;
    };

    session.set_active_chat = function (id) {
        $('.chat_tab_selected').removeClass('chat_tab_selected');
        if (!id || id == current.active_chat_id) {
            current.active_chat_id = null;
            $('#textbox')[0].disabled = true;
            $('#active_chat').addClass('no_active_chat');
        } else {
            $('#chat' + id).addClass('chat_tab_selected');
            current.active_chat_id = id;
            $('#textbox')[0].disabled = false;

            $('#active_chat').children().replaceWith(current.active_chats[id]);
            $('#active_chat').removeClass('no_active_chat');
            $('#chat' + id).removeClass('notification');
        }
        session.populate_member_list(current.active_chat_id);
    };

    function add_new_chat(data) {
        if (data.id in current.active_chats) {
            return data;
        }
        current.active_chats[data.id] = $("<div class='chat_content'></div>");
        window.ui.add_chat(data.id, data.name);
        session.set_active_chat(data.id);

        post_state();

        return data;
    }

    session.add_friend_id = function (friend_id) {
        return window.api.get_username(friend_id)
        .then(function (data) {
            data.id = friend_id;
            state.friends[data.id] = data;
            return add_new_chat(data);
        });
    };

    session.add_friend_name = function (friend_name) {
        return window.api.get_userid(friend_name)
        .then(function (data) {
            data.name = friend_name;
            state.friends[data.id] = data;
            return add_new_chat(data);
        });
    };

    session.populate_group_options = function (group_id) {
        var group = group_id ? state.groups[group_id] : null;

        var list = $('#add_group_members_inner');
        list.children().remove();

        for (var friend in state.friends) {
            var o = $('<div id="mem' + friend + '" class="group_mem_select">' + state.friends[friend].name + '</div>');

            if (group && friend in group.members) {
                o.addClass('group_mem_toggled');
            }

            list.append(o);
        }
    };

    session.populate_member_list = function (id) {
        // Strictly speaking this should be in window.ui, but this depends on so
        // many values in window.session that it was easier to do here.

        var list = $('#current_chat_member_list');
        list.children().remove();

        var options = $('#current_chat_member_options');
        options.children().remove();

        var leave = $('<div class="chat_option ui_button">Leave</div>');
        var settings = $('<div class="chat_option ui_button">Group Settings</div>');

        leave.click(() => session.leave_chat(id));
        settings.click(() => session.group_settings(id));

        if (!id) {
            $('#current_chat_members').addClass('no_current_chat');
            return;
        }
        $('#current_chat_members').removeClass('no_current_chat');

        options.append(leave);

        if (id in state.friends) {
            list.append($('<div id="group_mem' + current.user_id + '">' + current.user + '</div>'));
            list.append($('<div id="group_mem' + id + '">' + state.friends[id].name + '</div>'));
        } else if (id in state.groups) {
            var group = state.groups[id];
            for (var member in group.members) {
                list.append($('<div id="group_mem' + member + '">' + group.members[member].name + '</div>'));
            }
            options.append(settings);
        }
    };

    session.leave_chat = function (id) {
        session.dispatch_message(id, 'leave', 'so long and thanks for all the fish');
        $('#chat' + id).remove();
        
        if (id in state.groups) {
            delete state.groups[id];
        } else if (id in state.friends) {
            delete state.friends[id];
        }

        session.set_active_chat(null);

        post_state();
    };

    session.group_settings = function (id) {
        session.populate_group_options(id);
        ui.show_group_options(id, state.groups[id].name);
    };

    session.create_group_chat = function (name, group_id, key_id, members) {
        if (!name) {
            name = 'Group ' + group_id.substr(0,4);
        }

        var group;
        var old = true;
        if (group_id in state.groups) {
            group = state.groups[group_id];
            group.current_key_id = key_id;
        } else {
            var group = {
                name: name,
                id: group_id,
                keys: {},
                current_key_id: key_id,
            };
            old = false;
        }

        var key = gen_random_symmetric_key();
        group.keys[key_id] = key;

        var member_data = {};
        member_data[current.user_id] = {
            id: current.user_id,
            name: current.user,
        };
        for (var i in members) {
            member_data[members[i]] = state.friends[members[i]];
        }
        group.members = member_data;

        state.groups[group_id] = group;

        for (var i in members) {
            if (i != current.user_id) {
                session.send_group_invite(members[i], group_id, old);
            }
        }

        if (old) {
            session.set_active_chat(null);
            session.set_active_chat(group_id);
            post_state();
        } else {
            add_new_chat(group);
            session.websocket.socket.emit('listen', { ids: [group_id] });
        }
    }

    session.group_settings_create = function(group_id) {
        window.ui.hide_overlay();

        var selected = $('.group_mem_toggled');
        var members = [];

        for (var i = 0; i < selected.length; i++) {
            members.push(selected[i].id.replace('mem', ''));
        }

        var p;
        if (!group_id) {
            p = window.api.create_group()
            .then(function (new_group_id) {
                group_id = new_group_id;
                return window.api.create_group_key_id()
            });
        } else {
            p = window.api.create_group_key_id();
        }
        p.then(function (key_id) {
            session.create_group_chat(
                $('#group_name').val(),
                group_id,
                key_id,
                members
            );
        });
    };

    session.get_friend_key = function (friend_id) {
        var friend = state.friends[friend_id];
        return window.api.get_public_key(friend.key_id)
            .then(pub_key => ({ id: friend.key_id, rsa_pub: pub_key }));
    };

    session.websocket = {};

    session.websocket.connect = function () {
        var socket = io.connect(_websocket_target);
        session.websocket.socket = socket;

        socket.emit('listen', { ids: [current.user_id] });

        socket.on('message', function (data) {
            data = JSON.parse(data);

            if (data.sender_id == current.user_id) {
                return;
            }

            session.receive_message(data.sender_id, data.recipient_id, data.key_id, data.cryptotext);
        });
    }

    // Dispatches both group and personal messages based on given target ID
    session.websocket.send = function (target, data) {
        if (target in state.friends) {
            return session.websocket.send_personal(target, data);
        } else if (target in state.groups) {
            return session.websocket.send_group(target, data);
        }
    };

    // Sends a message encrypted with latest known group key
    session.websocket.send_group = function (group_id, data) {
        var group = state.groups[group_id];
        return sym_encrypt(group.keys[group.current_key_id], data)
        .then(function (ciphertext) {
            return session.websocket.socket.emit(
                'message',
                {
                    sender_id: current.user_id,
                    recipient_id: group_id,
                    key_id: group.current_key_id,
                    cryptotext: ciphertext,
                }
            );
        });
    };

    // Sends a message encrypted with recipient's public key
    session.websocket.send_personal = function (friend_id, data) {
        return session.get_friend_key(friend_id)
        .then(function (public_key) {
            return pgp_encrypt(public_key.rsa_pub, data)
            .then(function (ciphertext) {
                return session.websocket.socket.emit(
                    'message',
                    {
                        sender_id: current.user_id,
                        recipient_id: friend_id,
                        key_id: public_key.id,
                        cryptotext: ciphertext,
                    }
                );
            });
        });
    };

    return session;
})();

this.onload = function() {
    openpgp.initWorker({ path: 'lib/openpgp.worker.min.js' });

    $('.login_toggle').click(window.ui.toggle_login);

    $('#login_form').submit(function(e) {
        e.preventDefault();

        var login = $('#register_toggle').hasClass('toggle_hidden');
        var form = document.forms['login_form']

        if (login) {
            window.session.login(
                form['username'].value,
                form['password'].value
            );
        } else {
            window.session.register(
                form['username'].value,
                form['password'].value,
                form['password2'].value
            );
        }
    });

    $('#chat_sender').submit(function (e) {
        e.preventDefault();
        window.session.send_message($('#textbox')[0].value);
        $('#textbox')[0].value = '';
    });

    $('#user_search_form').submit(function (e) {
        e.preventDefault();

        var query = $('#user_search')[0].value;
        $('#user_search')[0].value = '';
        if (!query || query == window.session.get_current_username()) {
            return;
        }

        window.session.add_friend_name(query);
    });

    $('#add_group').click(function (e) {
        session.populate_group_options();
        ui.show_group_options();
    });

    $('#group_settings_close').click(function (e) {
        ui.hide_overlay();
    });

    $('#group_settings_create').click(function (e) {
        window.session.group_settings_create();
    });

    $('#username').on('input', function (e) {
        var text = $(this).val();

        if (text) {
            $(this).css('box-shadow', 'inset 0 0 4em ' + window.ui.stringToColor(text));
        } else {
            $(this).css('box-shadow', 'none')
        }
    });
}
