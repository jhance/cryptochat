'use strict';

var test = unitjs;


// Before running these tests, you should create this user if they don't
// exist already.
var preexisting_user = 'd';
var preexisting_password = 'd';

var test_pubkey = 'MIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgH4ehB5HymWZPuTqaAAq0KEV8+Qj' +
'o8675YTDi547CnEFFWby/97ucsqlq8uFSmyv57arBBls96Sqw927acavvogE+2Hv' +
'VK7FnMdEtwiPvOrWdA/Kyy2z4JuvBcUH67JFtKj3Z76dJeZFY1he/VnxMTC6QbLC' +
'Q0d1PtLhFIARBO6RAgMBAAE=';

var test_privkey = 'MIICWwIBAAKBgH4ehB5HymWZPuTqaAAq0KEV8+Qjo8675YTDi547CnEFFWby/97u' +
'csqlq8uFSmyv57arBBls96Sqw927acavvogE+2HvVK7FnMdEtwiPvOrWdA/Kyy2z' +
'4JuvBcUH67JFtKj3Z76dJeZFY1he/VnxMTC6QbLCQ0d1PtLhFIARBO6RAgMBAAEC' +
'gYBmIqx4IUeFs+89T5/d0s6WRcL31O0JtR0QwZPGFqZEFQvaDZytm4mXF0fjUBSP' +
'pH0Qb7qoJJkA39IzNV1aLGUte0ztD5of2OWf2u6udKAkdU28vRqkSzRUdBBJKdAC' +
'NWYKOqIXg8QR4LMyJXLVPBZwVFa97yWC4pOxn9bAnHdD4QJBALgZ12ZQ5Hi4HUuZ' +
'NoBUAKRzwke0WfP8dlLzsQowAnCJCkglkhCz1gxTv6Qgb/2NzK6ZZz4WDksqXrdT' +
'VzLmvB0CQQCvX72Bxh7F/k+7thmV8VYPYKL4T84AMckw8gP3l5GXrgdPzQCAY5DX' +
'ZmyhfKJqWsu6DI3R7vPNfG/TAfmceKoFAkEAlFOtqi/OhDzCzZ/5u1by1Yy2oRL7' +
'zoo/wwG4SB76ODURZaNvlalu8augIdpIoJ7YpGvQNMOMbJ8MhOPOw6IHOQJAAkF+' +
'A4xT2wIhbm2yOI6JJ9t7XSxRnwkE6H9Ec8vcUJIpf+pIJl6hf1PO8+jRggK2+iat' +
'MFUdh7ghDBrLZirbLQJAI9humnQ1vlNAWb2tOZStlG6aM3CSMuG3TfOt+cCdV/jY' +
'Qa6y70cA9pyGRknkAVgVlsgzc6ALdHzJ5RuMrlnc5Q==';

describe('setting up tests', function() {
    it('logging in preexisting user', function(done) {
        session.login(preexisting_user, preexisting_password);
        done()
    })
});

describe('api Tests', function() {
    it('not null', function() {
        test.object(api).isNotEqualTo(undefined);
    }),
    it('get_userid', function(done) {
        test.object(api).hasProperty('get_userid');
        api.get_userid('d').done(function (data) {
            test.string(data.id).isNotEqualTo(undefined);
            test.string(data.key_id).isNotEqualTo(undefined);
            done();
        }).fail(function (data) {
            done('Error response from api.get_userid');
        });
    }),
    it('get_state', function(done) {
        test.object(api).hasProperty('get_state');
        setTimeout(done, 5000);
        api.get_userid('d').done(function (data) {
            var key_id = data.key_id;
            api.get_state(key_id).then(function (data) {
                done();
            }).fail(function() {
                test.fail('Error response from api.get_state');
            });
        }).fail(function() {
            test.fail('Error response from api.get_userid');
        });
    }),
    it('get_public_key', function(done) {
        test.object(api).hasProperty('get_public_key');
        api.get_userid('d').done(function (data) {
            var key_id = data.key_id;
            api.get_public_key(key_id).then(function (data) {
                test.string(data).contains('PUBLIC KEY BLOCK');
                done();
            }).fail(function() {
                test.fail('Error response from api.get_user_id');
                done('err');
            });
        }).fail(function() {
            test.fail('Error response from api.get_public_key');
            done('err');
        });
    }),
    it('get_private_key', function(done) {
        test.object(api).hasProperty('get_private_key')
        api.get_userid('d').done(function (data) {
            var key_id = data.key_id;
            api.get_private_key(key_id).then(function (data) {
                test.object(data).hasProperty('root_key_cyphertext');
                done();
            }).fail(function() {
                test.fail('Error response from api.get_private_key.');
                done('err');
            });
        }).fail(function() {
            test.fail('Error response from api.get_user_id.');
            done('err');
        });     
    }),
    it('get_username', function(done) {
        test.object(api).hasProperty('get_username');
       
        api.get_username('junk_id').done(function (data) {
            test.fail('test failed, junk_id had non-error return value');
            done('err');
        }).fail(function() {
            // should have failed because the username is junk
            done();
        });
    }),
    it('create_group', function(done) {
        test.object(api).hasProperty('create_group');
        
        api.create_group().done(function(data) {
            done();
        }).fail(function() {
            test.fail('Error response from api.create_group.')
            done('err');
        })
    })
});

describe('session Tests', function() {
	it('not null', function() {
	    test.object(session).isNotEqualTo(undefined);
	}),
	it('register', function() {
	    test.object(session).hasProperty('register');
	    session.register('d', 'd', 'd');
	    current = session.get_current_data();
	    test.assert.equal('d', current.user);
	}),
	it('login', function() {
	    test.object(session).hasProperty('login');
	    session.login('d', 'd');
	    current = session.get_current_data();
	    test.assert.equal('d', current.user);
	}),
	it('add_friend_name', function() {
	    test.object(session).hasProperty('add_friend_name');
	    session.add_friend_name('adam');
	}),
	it('set_active_chat', function() {
	    test.object(session).hasProperty('set_active_chat');
	    friend_id = session.get_friend_key('adam');
	    session.set_active_chat(friend_id);
	    current = session.get_current_data();
	    test.assert.equals(current.active_chat_id, friend_id);
	}),
	it('send_message', function() {
	    test.object(session).hasProperty('send_message');
	    session.send_message('here is some sample text');
	}),
	it('add_friend_id', function() {
	    test.object(session).hasProperty('add_friend_id');
	    friend_key = session.get_friend_key('adam');
	    session.add_friend_id(friend_key);
	}),
	it('create_group_chat', function() {
	    test.object(session).hasProperty('create_group_chat');
	    session.create_group_chat('new group', 'group_id', 'key_id', []);
	    current = session.get_current_data();
	    test.assert(current.active_chats.indexOf('group_id') > -1);
	}),
	it('get_friend_key', function() {
	    test.object(session).hasProperty('get_friend_key');
	    session.get_friend_key('adam');
	})
});

