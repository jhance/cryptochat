# websocket.app
#
# Sets up the flask application for the websocket server or http server
# (both run with this, but as separate processes)
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_socketio import SocketIO

def create_app(name, settings_override={}):
    app = Flask(name)
    app.config.update(settings_override)

    return app
    
def create_db(app):
    return SQLAlchemy(app)

# CHANGE THIS IN PROD
settings = {'SQLALCHEMY_DATABASE_URI':'postgres://localhost/cryptochat'}
app = create_app(__name__, settings)

socketio = SocketIO(app)
db = create_db(app)
