from common.app import socketio, app
from websocket.core import *

if __name__ == '__main__':
    socketio.run(app, port=5005)
