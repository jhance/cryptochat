from abc import ABCMeta
from common.app import socketio
from websocket.consumer import *

from flask_socketio import join_room, leave_room

message_consumers = [] # type : List[MessageConsumer]
message_consumers.append(NotifyingConsumer())
message_consumers.append(DatabaseConsumer())

@socketio.on('message')
def handle_message(json_blob):
    for consumer in message_consumers:
        consumer.handle_message(WebsocketMessage.from_json_blob(json_blob))

# NOTE: @socketio.on converts things that were functions into odd things.
#
# Hence we define the actual functionality separately for testing.
def handle_listen_(json_blob):
    for i in json_blob['ids']:
        join_room(i)

def handle_ignore_(json_blob):
    for i in json_blob['ids']:
        leave_room(i)

@socketio.on('listen')
def handle_listen(json_blob):
    return handle_listen_(json_blob)

@socketio.on('ignore')
def handle_ignore(json_blob):
    return handle_ignore_(json_blob)
