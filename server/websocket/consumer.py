from abc import ABCMeta, abstractmethod
from common.app import db
from common.models.account import Account
from common.models.group import Group
from common.models.message import Message
from common.models.public_key import PublicKey

import json

from flask_socketio import emit


class WebsocketMessage:
    """Storage class for a single message. This is only the part of the
    message that is actually visible to the server, with no concern
    as to what the crypototext might actually contain.

    WARNING: There is redundancy here! We keep the json_blob as-is because
    when we process the message in the notification sender, we want to
    simply forward the message as-is to all consumers.
    """
    def __init__(self, sender_id, recipient_id, key_id, cryptotext):
        self.sender_id = sender_id
        self.recipient_id = recipient_id
        self.key_id = key_id
        self.cryptotext = cryptotext

    def to_json(self):
        return json.dumps({
            'sender_id': self.sender_id,
            'recipient_id': self.recipient_id,
            'key_id': self.key_id,
            'cryptotext': self.cryptotext,
        })

    @staticmethod
    def from_json_blob(data):
        print(data)
        return WebsocketMessage(
            data['sender_id'],
            data['recipient_id'],
            data['key_id'],
            data['cryptotext'],
        )


class MessageConsumer(metaclass=ABCMeta):
    @abstractmethod
    def handle_message(self, message: WebsocketMessage):
        """Handle a message in whichever way the consumer is responsible for.

        Examples include sending the message to anyone listening on the recipient
        side or serializing the message (to a database).
        """
        pass


class NotifyingConsumer(MessageConsumer):
    def __init__(self):
        pass

    def handle_message(self, message):
        # The websocket driver handles converting the recipient ids into rooms
        #
        # Generally, users will join the room corresponding to themselves and any
        # group they are a part of.
        print(message.to_json())
        emit('message', message.to_json(), room=message.recipient_id)


class DatabaseConsumer(MessageConsumer):
    def __init__(self):
        pass

    def handle_message(self, message):
        recipient = Group.query.get(message.recipient_id)
        sender = Account.query.get(message.sender_id)
        public_key = PublicKey.query.get(message.key_id)
        message = Message(
            recipient=recipient,
            sender=sender,
            cryptotext=message.cryptotext,
            key=public_key,
        )
        db.session.add(message)
        db.session.commit()
