from common.app import app, db
from common.models.public_key import PublicKey
from common.models.group import Group

from flask import abort, jsonify, request
import uuid

def handle_group_create_id():
    group = Group()
    db.session.add(group)
    db.session.commit()

    return jsonify({'id': str(group.id)}), 200

def handle_group_create_key():
    key = PublicKey(rsa_pub=None)
    db.session.add(key)
    db.session.commit()

    return jsonify({'id': str(key.id)}), 200
