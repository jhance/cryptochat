from flask import abort, request, jsonify

from common.app import db
from common.models.account import Account

import uuid

def handle_state_post(account_id):
    """Handle a state update. Fetch the account, update its state.

    Interprets the new state as being the body of the HTTP POST.
    """
    account = Account.query.get(account_id)
    if not account:
        abort(404)

    account.state = request.json['state']
    db.session.add(account)
    db.session.commit()

    return ''

def handle_state_get(account_id):
    """Replies with a HTTP body with the state of the user.

    This is normally encrypted and so they will need to decrypt
    it to access their state.
    """
    account = Account.query.get(account_id)
    if not account:
        abort(404)

    return jsonify({'state': account.state}) or ''
