from common.app import app, db
from common.models.account import Account
from common.models.group import Group
from common.models.public_key import PublicKey
from common.lib.account import make_account

from flask import abort, jsonify, request
import uuid

def handle_user_create():
    json_blob = request.json
    if not json_blob:
        abort(400)

    account = make_account(
        username=json_blob['username'],
        rsa_pub=json_blob['public_key'],
        root_key_ciphertext=json_blob['root_key_ciphertext'],
        state=json_blob.get('state'),
    )
    db.session.commit()

    return jsonify({'id': account.id}), 200

def handle_user_by_id(account_id):
    print(account_id)
    account = Account.query.get(account_id)
    if not account: 
        abort(404)

    return jsonify({'name': account.username, 'key_id': str(account.key_id) }), 200

def handle_user_by_name(username):
    account = Account.query.filter(Account.username == username).first()
    if not account:
        abort(404)

    return jsonify({'id': account.id, 'key_id': str(account.key_id) }), 200

def handle_find_users_by_prefix(username):
    accounts = Account.query.filter(Account.username.like('{}%'.format(username))).limit(100).all()
    account_names = [a.username for a in accounts]
    return jsonify({'names': account_names}), 200
