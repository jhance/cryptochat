from flask import abort, jsonify
from common.app import app, db
from common.models.account import Account
from common.models.public_key import PublicKey

def handle_public_get(public_key_id):
    public_key = PublicKey.query.get(public_key_id)
    if not public_key:
        abort(404)

    return jsonify({'public_key': public_key.rsa_pub}), 200

def handle_private_get(account_id):
    account = Account.query.get(account_id)
    if not account:
        abort(404)

    return jsonify({'root_key_ciphertext': account.root_key_ciphertext}), 200
