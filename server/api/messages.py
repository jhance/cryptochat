from common.app import app, db
from common.models.account import Account
from common.models.group import Group
from common.models.message import Message
from common.models.public_key import PublicKey
from common.lib.account import make_account

from flask import abort, jsonify, request
import uuid

from datetime import datetime

# Ignore offset by now.
#
# NOTE: offset should probably correspond to a timetstamp, not to an integral
# ordering, unless we also have an integral ordering for the messages.
def handle_message_index(group_id, offset):
    group = Group.query.get(group_id)
    if not group:
        abort(404)

    offset_timestamp = datetime.fromtimestamp(float(offset))
    messages = group.messages_received.filter(Message.timestamp < offset_timestamp).order_by(Message.timestamp).limit(100).all()
    return jsonify({'messages': [convert_message(m) for m in messages]}), 200

def handle_conversation_index(sender_group_id, receiver_group_id, offset):
    sender_group = Group.query.get(sender_group_id)
    receiver_group = Group.query.get(receiver_group_id)
    if not sender_group or not receiver_group:
        abort(404)

    offset_timestamp = datetime.fromtimestamp(float(offset))
    messages = Message.query.filter(
        Message.timestamp < offset_timestamp
    ).filter(
        ((Message.recipient_id == receiver_group.id) & (Message.sender_id == sender_group.id)) |
        ((Message.recipient_id == sender_group.id) & (Message.sender_id == receiver_group.id))
    ).order_by(Message.timestamp).limit(100).all()
    return jsonify({'messages': [convert_message(m) for m in messages]}), 200

def convert_message(message):
    return {
        'timestamp': message.timestamp,
        'sender_id': message.sender.id,
        'key_id': message.key.id,
        'cryptotext': message.cryptotext,
    }
