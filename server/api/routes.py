from api.state import handle_state_get, handle_state_post
from api.keys import handle_public_get, handle_private_get
from api.users import *
from api.groups import *
from api.messages import *


def add_routes(app):
    """Add the routes to the app.
    """
    app.add_url_rule('/state/<uuid:account_id>', 'handle_state_get', handle_state_get, methods=['GET'])
    app.add_url_rule('/state/<uuid:account_id>', 'handle_state_post', handle_state_post, methods=['POST'])
    app.add_url_rule('/keys/public/<uuid:public_key_id>', 'handle_public_get', handle_public_get, methods=['GET'])
    app.add_url_rule('/keys/private/<uuid:account_id>', 'handle_private_get', handle_private_get, methods=['GET'])
    app.add_url_rule('/user/create', 'handle_user_create', handle_user_create, methods=['POST'])
    app.add_url_rule('/user/prefix/<string:username>', 'handle_find_users_by_prefix', handle_find_users_by_prefix, methods=['GET'])
    app.add_url_rule('/user/name/<uuid:account_id>', 'handle_user_by_id', handle_user_by_id, methods=['GET'])
    app.add_url_rule('/user/id/<string:username>', 'handle_user_by_name', handle_user_by_name, methods=['GET'])
    app.add_url_rule('/messages/<uuid:group_id>/<string:offset>', 'handle_message_index', handle_message_index, methods=['GET'])
    app.add_url_rule('/conversations/<uuid:sender_group_id>/<uuid:receiver_group_id>/<string:offset>', 'handle_conversation_index', handle_conversation_index, methods=['GET'])

    app.add_url_rule('/groups/id', 'handle_group_create_id', handle_group_create_id, methods=['POST'])
    app.add_url_rule('/groups/key', 'handle_group_create_key', handle_group_create_key, methods=['POST'])

    # Hack to avoid cross site request issues. Definitely not staying.
    app.add_url_rule('/', 'root', lambda: app.send_static_file('index.html'), methods=['GET'])
    app.add_url_rule('/test', 'tests', lambda: app.send_static_file('test.html'), methods=['GET'])
    app.add_url_rule('/js/<path:path>', 'js', lambda path: app.send_static_file('js/' + path), methods=['GET'])
    app.add_url_rule('/lib/<path:path>', 'lib', lambda path: app.send_static_file('lib/' + path), methods=['GET'])
    app.add_url_rule('/css/<path:path>', 'css', lambda path: app.send_static_file('css/' + path), methods=['GET'])
    app.add_url_rule('/img/<path:path>', 'img', lambda path: app.send_static_file('img/' + path), methods=['GET'])
