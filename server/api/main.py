from common.app import app
from api.routes import add_routes
from api.keys import *
from api.users import *
from api.state import *

if __name__ == '__main__':
    add_routes(app)
    app.run()
