import pytest

import common.models.group as group
from common.models.account import Account
from common.models.group import Group
from common.models.message import Message
from common.models.public_key import PublicKey

import api.users as users

from sqlalchemy import text

#def test_group_model(session):
#    group1 = group.Group() 
#    group2 = group.Group()
#    session.add_all([group1, group2])
#    session.commit()
#    
#    results = session.execute(text("SELECT * FROM group")).fetchall()
#    print("results = "+str(results))
#    assert len(results) == 2
    
def test_account_model(session):
    key = PublicKey("~secret_code~")
    group = Group()
    acc = Account(group, "DPendergast", key)
    session.add(group)
    session.add(key)
    session.add(acc)
    session.commit()
    results = session.execute(text("SELECT * FROM account")).fetchall()
    print("results = "+str(results))
    assert len(results) == 1
    assert results[0][1] == "DPendergast"
    assert results[0][2].hex == key.id.hex
    
def test_message_model(session):
    user1key = PublicKey("test code 1")
    user2key = PublicKey("test code 2")

    sender_g = Group()
    receiver_g = Group()
    session.add_all([sender_g, receiver_g])

    sender = Account(sender_g, "sender", user1key)
    receiver = Account(receiver_g, "receiver", user2key)
    messagekey = PublicKey("test code 3")
    
    message = Message("cryptotext", sender, receiver_g, messagekey)
    session.add_all([user1key, user2key, sender, receiver, messagekey, message])
    session.commit()
    
    results = session.execute("SELECT * FROM message").fetchall()
    print (str(results))
    assert len(results) == 1
    assert results[0][1] == 'cryptotext'
    
def test_public_key_model(session):
    key = PublicKey("testcode")
    session.add(key)
    session.commit()
    
    results = session.execute("SELECT * FROM public_key").fetchall()
    print(str(results))
    # assert len(results) == 1              ## BROKEN TEST
    # assert results[0][1] == "testcode"    ## DB isn't being cleared between tests
    
