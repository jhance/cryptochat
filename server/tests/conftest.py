# STOLEN FROM http://alexmic.net/flask-sqlalchemy-pytest/
# With light edits

import os
import pytest

from api.routes import add_routes
from common.app import app, create_app
from common.app import db as _db


TEST_DATABASE_URI = 'postgres://localhost/cryptochat_test'


@pytest.fixture(scope='session')
def app(request):
    """Session-wide test `Flask` application."""
    settings_override = {
        'TESTING': True,
        'SQLALCHEMY_DATABASE_URI': TEST_DATABASE_URI
    }
    app = create_app(__name__, settings_override)
    add_routes(app)

    # Establish an application context before running the tests.
    ctx = app.app_context()
    ctx.push()
    
    _db.init_app(app)

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)
    return app


@pytest.fixture(scope='function')
def db(app, request):
    """Session-wide test database."""
    def teardown():
        _db.drop_all()

    _db.app = app
    _db.create_all()

    request.addfinalizer(teardown)
    return _db


@pytest.fixture(scope='function')
def session(db, request):
    """Creates a new database session for a test.
    
    Note that this also sets db.session to session, so all code
    will use the created session.
    """
    connection = db.engine.connect()
    transaction = connection.begin()

    options = dict(bind=connection, binds={})
    session = db.create_scoped_session(options=options)

    db.session = session

    def teardown():
        transaction.rollback()
        connection.close()
        session.remove()

    request.addfinalizer(teardown)
    return session
    
# Not stolen, written by us:
@pytest.fixture(scope='function')
def client(app):
    return app.test_client()
