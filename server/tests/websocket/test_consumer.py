from websocket.consumer import WebsocketMessage, NotifyingConsumer, DatabaseConsumer
from common.models.public_key import PublicKey
from common.models.group import Group
from common.models.account import Account
import websocket.consumer
import json
import mock

def make_test_message(session):
    s_public_key = PublicKey(rsa_pub='xxx')
    r_public_key = PublicKey(rsa_pub='yyy')
    session.add(s_public_key)
    session.add(r_public_key)

    sender_g = Group()
    receiver_g = Group()
    session.add(sender_g)
    session.add(receiver_g)

    sender = Account(group=sender_g, username='sender', key=s_public_key)
    receiver = Account(group=receiver_g, username='receiver', key=r_public_key)
    session.add(sender)
    session.add(receiver)
    session.commit()

    return WebsocketMessage(
        sender_id = str(sender.id),
        recipient_id = str(receiver.id),
        key_id = str(receiver.key.id),
        cryptotext = 'secret',
    )

def test_websocket_message_serialization(session):
    message = make_test_message(session)
    message2 = WebsocketMessage.from_json_blob(json.loads(message.to_json()))

    assert message.sender_id == message2.sender_id
    assert message.recipient_id == message2.recipient_id
    assert message.key_id == message2.key_id
    assert message.cryptotext == message2.cryptotext

@mock.patch('websocket.consumer.emit')
def test_notifying_consumer(mock_emit, session):
    mock_emit.return_value = None
    message = make_test_message(session)
    consumer = NotifyingConsumer()
    consumer.handle_message(message)
    assert mock_emit.called_with('message', message.to_json(), room=message.recipient_id)

def test_database_consumer(session):
    message = make_test_message(session)
    consumer = DatabaseConsumer()
    consumer.handle_message(message)
    
    sender = Account.query.get(message.sender_id)
    receiver = Account.query.get(message.recipient_id)
    assert len(sender.messages_sent.all()) == 1
    assert len(receiver.group.messages_received.all()) == 1
