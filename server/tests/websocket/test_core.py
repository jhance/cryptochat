from websocket.core import *
import mock

@mock.patch('websocket.core.join_room')
def test_handle_listen(mock_join_room):
    handle_listen_({'ids': [1, 2]})
    assert mock_join_room.called_with(1)
    assert mock_join_room.called_with(2)

@mock.patch('websocket.core.leave_room')
def test_handle_ignore(mock_leave_room):
    handle_ignore_({'ids': [1, 2]})
    assert mock_leave_room.called_with(1)
    assert mock_leave_room.called_with(2)
