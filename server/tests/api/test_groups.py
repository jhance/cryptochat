from common.app import db
from common.models.group import Group
from common.models.public_key import PublicKey
from api.groups import *

import json

def test_create_group(client, session):
    response = client.post('/groups/id')
    assert response.status_code == 200
    group_id = json.loads(response.data.decode())['id']
    assert session.query(session.query(Group).filter(Group.id == group_id).exists())

def test_create_key(client, session):
    response = client.post('/groups/key')
    assert response.status_code == 200
    group_id = json.loads(response.data.decode())['id']
    assert session.query(session.query(PublicKey).filter(PublicKey.id == group_id).exists())

