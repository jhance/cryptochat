from common.app import db
from common.models.account import Account
from common.models.group import Group
from common.models.public_key import PublicKey
from api.state import *

import json

def test_users_create(client, session):
    data = { 'public_key': 'public key',
             'username': 'test user',
             'root_key_ciphertext': 'cipher',
           }

    response = client.post('/user/create', data=json.dumps(data), content_type='application/json')
    assert response.status_code == 200

def test_users_by_id(client, session):
    public_key = PublicKey("testkey")
    db.session.add(public_key)

    group = Group()
    db.session.add(group)

    account = Account(group, "test user", public_key)
    account.state = 'testing state'
    db.session.add(account)
    db.session.commit()

    response = client.get('/user/name/{}'.format(account.id))
    assert response.status_code == 200
    assert json.loads(response.data.decode())['name'] == 'test user'

def test_users_by_name(client, session):
    public_key = PublicKey("testkey")
    db.session.add(public_key)

    group = Group()
    db.session.add(group)

    account = Account(group, "testuser", public_key)
    account.state = 'testing state'
    db.session.add(account)
    db.session.commit()

    response = client.get('/user/id/testuser')
    assert response.status_code == 200
    assert json.loads(response.data.decode())['id'] == str(account.id)

def test_users_by_prefix(client, session):
    public_key = PublicKey("testkey")
    db.session.add(public_key)

    group = Group()
    db.session.add(group)

    account = Account(group, "testuser", public_key)
    account.state = 'testing state'
    db.session.add(account)
    db.session.commit()

    response = client.get('/user/prefix/test')
    assert response.status_code == 200
    assert json.loads(response.data.decode())['names'] == ['testuser']
