from common.app import db
from common.models.account import Account
from common.models.public_key import PublicKey
from common.lib.account import make_account
from api.state import *

import json

def test_state_get(client, session):
    account = make_account("test user", "testkey", state='testing state')
    db.session.commit()

    response = client.get('/state/{}'.format(account.id))
    assert response.status_code == 200
    assert json.loads(response.data.decode())['state'] == 'testing state'

def test_state_post(client, session):
    account = make_account("test user", "testkey", state='testing state')
    db.session.commit()

    response = client.post('/state/{}'.format(account.id), data=json.dumps(
        {'state': 'testing state 2'}), content_type='application/json')

    assert response.status_code == 200
    
    refreshed_account = Account.query.get(account.id)
    assert refreshed_account.state == 'testing state 2'
