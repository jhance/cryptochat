from common.app import db
from common.models.account import Account
from common.models.message import Message
from common.models.group import Group
from common.models.public_key import PublicKey
from common.lib.account import make_account
from api.state import *

from datetime import datetime

import json

def convert_message(message):
    return {
        'timestamp': message.timestamp,
        'sender_id': message.sender.id,
        'sender_username': message.sender.username,
        'key_id': message.key.id,
        'cryptotext': message.cryptotext,
    }

def test_message_index(client, session):
    sender = make_account('sender', 'sender_key')
    recipient = make_account('recipient', 'recipient_key')

    message = Message(
        sender = sender,
        recipient = recipient.group,
        cryptotext = 'blah',
        key = sender.key,
    )
    db.session.add(message)

    db.session.commit()

    response = client.get('/messages/{}/{}'.format(recipient.id, datetime.utcnow().timestamp()))
    assert response.status_code == 200
    assert len(json.loads(response.data.decode())['messages']) == 1

    response = client.get('/messages/{}/{}'.format(recipient.id, message.timestamp.timestamp()))
    assert response.status_code == 200
    assert len(json.loads(response.data.decode())['messages']) == 0

def test_conversation_index(client, session):
    sender = make_account('sender', 'sender_key')
    recipient = make_account('recipient', 'recipient_key')

    message1 = Message(
        sender = sender,
        recipient = recipient.group,
        cryptotext = 'blah',
        key = sender.key,
    )
    db.session.add(message1)
    message2 = Message(
        sender = recipient,
        recipient = sender.group,
        cryptotext = 'blah2',
        key = recipient.key,
    )
    db.session.add(message2)
    db.session.commit()
    response = client.get('/conversations/{}/{}/{}'.format(sender.id, recipient.id, datetime.utcnow().timestamp()))
    assert response.status_code == 200
    assert len(json.loads(response.data.decode())['messages']) == 2
