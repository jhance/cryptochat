package 'nginx' do
    action :install
end

cookbook_file '/etc/nginx/sites-enabled/default' do
    source 'nginx_config'
    mode '0755'
    owner 'root'
    group 'root'
    notifies :restart, 'service[nginx]'
end

service 'nginx' do
    action :start
end
