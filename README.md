# cryptochat
##### Adam Johnston, David Pendergast, Jared Hance

Cryptochat is a centralized chat server providing end-to-end encryption for its users. The general promise the server provides is that no user information besides ID and public key is ever readable in cleartext server-side. Key generation, encryption, etc are all handled by the client, which maintains a state that encrypted and synced to the server to allow for continuity across sessions. Further information regarding the public API specification and implemention details can be found [here](https://docs.google.com/a/case.edu/document/d/1I1lETfKVi9ofJ8ENJQKZ-OIxO4UOy3wGSN-YhABu7FU/edit?usp=sharing).

## System Requirements

Linux is highly recommended as setup will be easiest.

## Setting up postgresql

Install postgresql and create your relevant user (for your linux username) with ownership of the database 'cryptochat' and 'cryptochat_test'. Anonymous access from 127.0.0.1/32 must be permitted using 'trust' in `hba.conf` which can be located via the commands:
```
sudo su - postgres
psql
SHOW hba_file;
```

 The databases can be created via the commands:
```
sudo su - postgres -c "createdb cryptochat"
sudo su - postgres -c "createdb cryptochat_test"
```

If you are using ```systemd```, you can now start the postgres service with:
```
sudo systemctl start postgresql.service
```

The live database can be introspected with:
```
psql cryptochat
```

## Setting up the python env and running the API and websocket endpoints

Create a virtualenv with a default of ```-p python3``` and install the dependencies with ```pip install -r requirements.txt```.

Migrate the database with `alembic upgrade head`. (This step will fail if your postgres server is not running and configured with the proper database).

Start the HTTP API server with `python -m api.main` from the server folder. This will run on `127.0.0.1:5000`.

Start the Websocket server with `python -m websocket.main` from the server folder. This will run on `127.0.0.1:5005`.

Navigate your favorite browser to `127.0.0.1:5000`.

## Setting up nginx to serve over HTTPS

Browser security measures do not allow websites to use `window.crypto` or worker threads unless they are considered to be secure content. Locally served content (over `127.0.0.1`) is considered secure automatically, but to make the server public facing, you need to obtain a certificate for TLS/HTTPS. With the certificate and the associated key for your FQDN, install nginx and use the provided `nginx_conf` file in `/etc/nginx/sites-enabled/`. For this configuration to work, you will need to also install uwsgi and move the `cryptochat.ini` uwsgi config file to the `server/` folder and launch the API through uwsgi. The config will proxy HTTPS websocket requests to a locally running websocket endpoint at `127.0.0.1:5005` as `socket.io` does not natively support HTTPS/WSS.

While a self signed certificate can be used to enable `window.crypto` and worker threads, there is no guaruntee that all features being used will be enabled by the browser. Nginx and HTTPs are only necessary if hosting publically - if the server is being run locally, the setup is not needed.
